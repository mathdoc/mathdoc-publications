# Mathdoc publication

Wordpress pluging to display the Mathdoc publications hosted in HAL

## Summary

1. [How to install WordPress ?](#worpress)
2. [How to use the plugin ?](#plugin)

----

### **How to install WordPress ?** <a name="wordpress"></a>

To install WordPress please go to the web page : ```https://fr.wordpress.org/download/ ```
Once downloaded, mv the wordpress unzipped folder to your local Apache root folder (ex /var/www/html)
Go to the link : ```http://localhost/worpress/wp-admin/``` and follow the instructions

Install the Classic Widgets extension so that you can use the plugin in the Appearance menu

----

### **How to use the plugin ?** <a name="plugin"></a>

To be able to use the plugin, just copy paste the ```"publications"``` directory in ```wp-content/plugins```. 
Go to the Extensions page of your local wordpress admin site and activate the ```Mathdoc publications``` plugin


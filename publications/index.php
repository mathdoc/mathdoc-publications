<?php
/*
Plugin Name: Mathdoc publications
Plugin URI: http://www.mathdoc.fr/
Description: This plugin allows you to display the Mathdoc publications hosted in HAL
Version: 1.0.0
Author: Mathdoc
Author URI: http://www.mathdoc.fr/
*/

function mathdoc_publications_widget_init()
{
    register_widget('mathdoc_publications');
}

add_action('widgets_init', 'mathdoc_publications_widget_init');

class mathdoc_publications extends WP_Widget
{
    public $options = ['(ART OR COUV OR DOUV)', '(COMM OR LECTURE OR POSTER)', '(REPORT OR OTHER OR UNDEFINED)'];
    public $values = ['Publications', 'Supports de présentations', 'Rapports'];
    /*
     * Constructeur de la classe mathdoc_publications qui hérite de la classe WP_Widget
     */
    public function __construct()
    {
        parent::__construct(
        // L'id du widget
            'mathdoc_publications',

            // Le nom du widget qui va apparaitre dans l'interface utilisateur
            __('Mathdoc publications', 'mathdoc_publications_domain'),

            // Description du widget
            array('description' => __('Affiche les publications de Mathdoc hébergées dans HAL', 'mathdoc_publications_domain'),)
        );
    }

    /**
      * Back-end widget form.
      *
      * @see WP_Widget::form()
      *
      * @param array $instance Previously saved values from database.
      */
    public function form( $instance ) 
    {    
        $type = '';
        if (isset($instance["type"])) {
            $type = esc_attr( $instance['type'] );
        }
  
        echo '<p>';
        echo '<label for="' . $this->get_field_id('type') . '">' . _e('Type de publications:') . '</label>'; 
        echo '<select class="widefat" id="' . $this->get_field_id('type') . '" name="' . $this->get_field_name('type') . '">';

        for ($i = 0; $i < count($this->options); $i++) {
            echo '<option value="' . $i . '"';
            if ($i == $type) {
                echo ' selected';
            }
            echo '>' . $this->values[$i] . '</option>';
        }
        echo '</select>';
        echo '</p>';
    }

    /**
      * Sanitize widget form values as they are saved.
      *
      * @see WP_Widget::update()
      *
      * @param array $new_instance Values just sent to be saved.
      * @param array $old_instance Previously saved values from database.
      *
      * @return array Updated safe values to be saved.
      */
    public function update( $new_instance, $old_instance ) 
    {        
        $instance = $old_instance;
        $instance['type'] = strip_tags( $new_instance['type'] );
        return $instance;
    }

    /*
     * La fonction permet de définir ce que va devoir faire le widget
     *
     *
     * @param array $args Il s'agit d'un tableau de valeurs contenant plein de choses sur la page. Ces éléments sont récupérés en fonction de l'emplacement du widget
     * @param array instance Il s'agit d'un tableau de valeurs vide.
     */

    public function widget($args, $instance)
    {
        extract($args);
        echo $before_widget;

        $url = 'https://api.archives-ouvertes.fr/search/MATHDOC/?q=*:*&fl=title_s,authFullName_s,citationRef_s,uri_s,files_s,fileAnnexes_s,publisherLink_s,docType_s,thumbId_i&sort=publicationDate_s%20desc&rows=3';
        $url .= '&fq=docType_s:' . $this->options[$instance['type']];
        $json_response = json_decode(wp_remote_retrieve_body(wp_remote_get($url)), true);

        echo '<div class="mathdoc_div_publications">';

        if (is_null($json_response) or !isset($json_response['response']) or !isset($json_response['response']['docs'])) {
            echo '<p>HAL is not responding. Please try again later</p>';
            echo '</div>';
            return;
        }

        $docs = $json_response['response']['docs'];

        foreach ($docs as $publication) {
            $link = '';
            if (isset($publication["files_s"]) and !empty($publication["files_s"])) {
                $link = $publication["files_s"][0];
            } elseif (isset($publication["fileAnnexes_s"]) and !empty($publication["fileAnnexes_s"])) {
                $link = $publication["fileAnnexes_s"][0];
            } elseif (isset($publication["publisherLink_s"]) and !empty($publication["publisherLink_s"])) {
                $link = $publication["publisherLink_s"][0];
            }
            echo '<div class="mathdoc_publication">';
                if (isset($publication["thumbId_i"]) and !empty($publication["thumbId_i"])) {
                    echo '<img class="mathdoc_publication_img" src="https://thumb.ccsd.cnrs.fr/' . $publication["thumbId_i"] . '">';
                } elseif ($instance['type'] == 0) {
                    echo '<img class="mathdoc_publication_img" src="https://thumb.ccsd.cnrs.fr/9094994">';
                } elseif ($instance['type'] == 1) {
                    echo '<img class="mathdoc_publication_img" src="https://thumb.ccsd.cnrs.fr/8367762">';
                } elseif ($instance['type'] == 2) {
                    echo '<img class="mathdoc_publication_img" src="https://thumb.ccsd.cnrs.fr/9086997">';
                }

                echo '<div class="mathdoc_ctn_text">';
        
                    if (empty($link)) {
                        echo '<h3 class="mathdoc_publication_title">' . $publication["title_s"][0] . '</h3>';
                    } else {            
                        echo '<h3 class="mathdoc_publication_title"><a href="' . $link . '">' . $publication["title_s"][0] . '</a></h3>';
                    }

                    $author_text = '';
                    foreach ($publication["authFullName_s"] as $author) {
                        if (!empty($author_text)) {
                            $author_text .= ', ';
                        } 
                        $author_text .= $author;
                    }
                    echo '<p class="mathdoc_publication_authors">' . $author_text . '</p>';

                    echo '<p class="mathdoc_publication_citation">' . $publication["citationRef_s"] . '</p>';
		    if(ICL_LANGUAGE_CODE=='en'){
                        echo '<a class="mathdoc_publication_link" href="' . $publication["uri_s"] . '">More information</a>';
                    } else {
                        echo '<a class="mathdoc_publication_link" href="' . $publication["uri_s"] . '">En savoir plus</a>';
                    }

                echo '</div>';
            echo '</div>';
        }
  
        echo '</div>';
        echo $after_widget;
    }
}
